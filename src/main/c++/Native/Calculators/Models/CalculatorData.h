//
//  CalculatorData.h
//  LCA.Kernel
//
//  Created by francois saab on 2/18/18.
//  Copyright © 2018 fsaab. All rights reserved.
//

#ifndef CalculatorData_h
#define CalculatorData_h

#include <stdio.h>

#include <string>
#include <vector>
#include "../../LCAModels/CalcImpactFactorItem.hpp"
#include "../../LCAModels/ExchangeItem.hpp"
#include "../../DAL/ParameterTable.h"
using namespace std;
#include "AppSettings.hpp"
#include "../../LCAModels/LCAIndexes.hpp"
#include "ImpactStat.hpp"
#include <boost/mpi.hpp>
#include <unordered_map>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/unordered_map.hpp>
#include <boost/serialization/string.hpp>

typedef exprtk::expression<double> expression_t;

struct CalculatorData
{
  private:
  public:
    friend class boost::serialization::access;

    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
        ar &A_exchanges;
        ar &B_exchanges;
        ar &Q_cells;

        ar &lcaIndexes;
        ar &demand_demandVectorArray;

        ar &impactStats;

        ar &impactCategoriesLength;
        ar &elementaryFlowsLength;
        ar &intermediateFlowsLength;
        ar &processesLength;
        ar &A_size;
        ar &B_size;
        ar &Q_size;
        //long P_size;

        ar &A_unc_size;
        ar &B_unc_size;

        ar &A_unc_exchanges;
        ar &B_unc_exchanges;
        ar &Q_unc_cells;
        //ar & ASamplesIds_indices;
        //ar & BSamplesIds_indices;
    }

    AppSettings settings;

    vector<ExchangeItem> A_exchanges;
    vector<ExchangeItem> B_exchanges;
    vector<CalcImpactFactorItem> Q_cells;

    vector<ExchangeItem> A_unc_exchanges;
    vector<ExchangeItem> B_unc_exchanges;
    vector<CalcImpactFactorItem> Q_unc_cells;

    unordered_map<long, long> ASamplesIds_indices;
    /*unordered_*/ map<long, long> BSamplesIds_indices;

    LCAIndexes lcaIndexes;

    vector<double> demand_demandVectorArray;
    vector<ImpactStat> impactStats;

     /*<scope_owner,expresion>*/
    /*unordered_*/map<string, expression_t> expressionsTable;
    //expression_t expressionTable;

    long impactCategoriesLength = 0;
    long elementaryFlowsLength = 0;
    long intermediateFlowsLength = 0;
    long processesLength = 0;

    long A_size = 0;
    long B_size = 0;
    long Q_size = 0;
    //long P_size;

    long A_unc_size = 0;
    long B_unc_size = 0;

    static std::vector<std::string> split(std::string input, std::string sep)
    {
        vector<string> strs;
        boost::split(strs, input, boost::is_any_of(sep));
        return strs;
    }

    CalculatorData() {}

    CalculatorData(AppSettings settings_,
                   vector<ExchangeItem> A_exchanges_,
                   vector<ExchangeItem> B_exchanges_,
                   vector<CalcImpactFactorItem> Q_Cells_,
                   LCAIndexes lcaIndexes_,
                   vector<double> demand_demandVectorArray_,
                   vector<ImpactStat> impactStats_)
    {
        settings = settings_;
        A_exchanges = A_exchanges_;
        B_exchanges = B_exchanges_;
        Q_cells = Q_Cells_;
        demand_demandVectorArray = demand_demandVectorArray_;
        lcaIndexes = lcaIndexes_;
        impactStats = impactStats_;
    }

    void loadSamplesIndices()
    {

        FileUtils fileUtils;
        std::stringstream ss_A_samples_indices_path;
        ss_A_samples_indices_path << "data/all_exchanges_A_indices.txt";

        if (fileUtils.exists(ss_A_samples_indices_path.str()))
        {

            vector<string> all_exchanges_A_strs = split((fileUtils.readTextFromFile(
                                                            ss_A_samples_indices_path.str())[0]),
                                                        ",");
            long Asamplessize = all_exchanges_A_strs.size();
            for (long ind_sample = 0; ind_sample < Asamplessize; ind_sample++)
            {
                string exchid_str = all_exchanges_A_strs[ind_sample];
                if (exchid_str != "")
                {
                    ASamplesIds_indices[stol(exchid_str)] = ind_sample;
                }
            }
        }

        std::stringstream ss_B_samples_indices_path;
        ss_B_samples_indices_path << "data/all_exchanges_B_indices.txt";

        if (fileUtils.exists(ss_B_samples_indices_path.str()))
        {

            vector<string> all_exchanges_B_strs = split((fileUtils.readTextFromFile(
                                                            ss_B_samples_indices_path.str())[0]),
                                                        ",");

            long Bsamplessize = all_exchanges_B_strs.size();

            for (long ind_sampleB = 0; ind_sampleB < Bsamplessize; ind_sampleB++)
            {
                string exchid_str = all_exchanges_B_strs[ind_sampleB];
                if (exchid_str != "")
                {

                    BSamplesIds_indices[stol(exchid_str)] = ind_sampleB;
                }
            }
        }
    }

    void loadLengths()
    {

        impactCategoriesLength = lcaIndexes.ImpactCategoryIndexLength();
        elementaryFlowsLength = lcaIndexes.ElementaryFlowsIndexLength();
        intermediateFlowsLength = lcaIndexes.IntermediateFlowsIndexLength();
        processesLength = lcaIndexes.ProcessesIndexLength();

        A_size = A_exchanges.size();
        B_size = B_exchanges.size();
        Q_size = Q_cells.size();
        //P_size = A_exchanges.size();
    }

    void LoadUncertainCells()
    {

        for (auto &&cell : A_exchanges)
        {
            if (cell.exch.uncertaintyType != 0)
            {
                A_unc_exchanges.push_back(cell);
            }
        }

        for (auto &&cell : B_exchanges)
        {
            if (cell.exch.uncertaintyType != 0)
            {
                B_unc_exchanges.push_back(cell);
            }
        }

        for (auto &&cell : Q_cells)
        {
            if (cell.imf.uncertaintyType != 0)
            {
                Q_unc_cells.push_back(cell);
            }
        }
    }

    void LoadAndWriteUncertainCells()
    {
        FileUtils fileUtils;

        std::stringstream ss_A_samples_ids_path;
        ss_A_samples_ids_path << "./data/all_exchanges_A_indices.txt";
        std::stringstream ss_A_samples_ids;
        for (auto &&cell : A_exchanges)
        {
            if (cell.exch.uncertaintyType != 0)
            {
                A_unc_exchanges.push_back(cell);
                A_unc_size++;
                ss_A_samples_ids << cell.exch.exchangeId << ",";
            }
        }

        fileUtils.writeTextToFile(ss_A_samples_ids.str(), ss_A_samples_ids_path.str());

        std::stringstream ss_B_samples_ids_path;
        ss_B_samples_ids_path << "./data/all_exchanges_B_indices.txt";
        std::stringstream ss_B_samples_ids;

        for (auto &&cell : B_exchanges)
        {
            if (cell.exch.uncertaintyType != 0)
            {
                B_unc_exchanges.push_back(cell);
                B_unc_size++;
                ss_B_samples_ids << cell.exch.exchangeId << ",";
            }
        }

        fileUtils.writeTextToFile(ss_B_samples_ids.str(), ss_B_samples_ids_path.str());

        for (auto &&cell : Q_cells)
        {
            if (cell.imf.uncertaintyType != 0)
            {
                Q_unc_cells.push_back(cell);
            }
        }
    }
};

#endif /* CalculatorData_h */
