/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Francois Saab
 * Mail: saab.francois@gmail.com, francois.saab.1@ens.etsmtl.ca
 * Date: 1/1/2017
 *
 * Copyright © 2017 Francois Saab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/

#ifndef LCAEdgeCompact_hpp
#define LCAEdgeCompact_hpp

#include <stdio.h>
#include "../Calculators/Models/AppSettings.hpp"

#include "../Utilities/ResultsUtilsSingle.hpp"

#include "../LCAModels/LCAIndexes.hpp"

class LCAEdgeCompact {
public:

    long InputExchangeId;
    long ProducerExchangeId;
    long ProcessSrcId;
    long ProcessDestId;
    long FlowId;

    LCAEdgeCompact(long InputExchangeId_,
            long ProducerExchangeId_,
            long ProcessSrcId_,
            long ProcessDestId_,
            long FlowId_) {

        InputExchangeId = InputExchangeId_;
        ProducerExchangeId = ProducerExchangeId_;
        ProcessSrcId = ProcessSrcId_;
        ProcessDestId = ProcessDestId_;
        FlowId = FlowId_;
    }


};


#endif /* LCAEdgeCompact_hpp */
