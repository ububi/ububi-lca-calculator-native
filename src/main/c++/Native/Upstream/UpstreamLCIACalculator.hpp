//
//  UpstreamLCIA.hpp
//  LCA.Kernel
//
//  Created by francois saab on 2/26/18.
//  Copyright © 2018 fsaab. All rights reserved.
//

#ifndef UpstreamLCIACalculator_hpp
#define UpstreamLCIACalculator_hpp

#include <stdio.h>

#include "../Factories/TechnologyMatrixFactorySingle.hpp"
#include "../Factories/InterventionMatrixFactorySingle.hpp"
#include "../Factories/CharacterisationMatrixFactory.hpp"
#include <omp.h>

#include "../Calculators/Models/AppSettings.hpp"
#include "../Utilities/ResultsUtilsSingle.hpp"
#include "../LCAModels/LCAIndexes.hpp"

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/SparseCore>
#include <eigen3/Eigen/SparseLU>
#include <eigen3/Eigen/IterativeLinearSolvers>
#include <eigen3/Eigen/Dense>
//#include <Eigen>
//#include "../libs/libMUMPS.hpp"
//#include <Eigen/UmfPackSupport>
using Eigen::BiCGSTAB;
using Eigen::SparseMatrix;
using Eigen::VectorXd;
typedef Eigen::Triplet<double> Triplet;
typedef Eigen::SparseMatrix<double> SMatrix;

#include "../Messaging/LCAException.hpp"
#include "LinearGraphUtil.hpp"
#include "../Reports/ContributionAnalysisReport.hpp"
#include "../Graph/GraphData.h"
#include "../libs/libQBAInvEigen.hpp"
//#include "../Optimisations/libQBAInv.hpp"

class UpstreamLCIACalculator
{
    AppSettings settings;
    CalculatorData *calculatorData;
    LCADB *lcadb;
    GraphData *graph;

  public:
    UpstreamLCIACalculator(
        AppSettings _settings,
        CalculatorData *_calculatorData,
        LCADB *_lcadb,
        GraphData *_graph) : settings(_settings), calculatorData(_calculatorData), lcadb(_lcadb), graph(_graph)
    {
    }

    UpstreamLCIACalculator()
    {
    }

    void run(double * lciaR)
    {
        Eigen::MatrixXd QBAinv = libQBAInvEigen::calculateByTransposeSystemBICGSTAB(calculatorData,settings);

        LinearGraphUtils linearGraphUtils(lcadb, graph, settings);
        vector<ContributionTreeNode> tree = linearGraphUtils.buildLinearTree(settings.RootProcessId, calculatorData);

        ContributionAnalysisReport report(settings, calculatorData, lcadb);
        SMatrix ASparse = TechnologyMatrixFactorySingle::build( settings,calculatorData);

        report.printLCIA(graph, ASparse, lciaR, QBAinv,tree);
    }
};

#endif /* UpstreamLCIACalculator_hpp */
