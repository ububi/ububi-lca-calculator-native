mpic++ -pipe  -D_JNI_IMPLEMENTATION_  -m64  -fPIC ./LCAKernel_LCAKernel.cpp  -Ilibs/ -I$BOOST_HOME/  -std=c++11 -O3 -fopenmp -DBOOST_SYSTEM_NO_DEPRECATED \
            -I"$JAVA_HOME/include/"  \
            -I"$JAVA_HOME/include/linux/" \
            -L $BOOST_HOME/stage/lib -lgsl -lgslcblas -lm  -lboost_mpi -lboost_serialization  -shared -o libLCAKernel.so
