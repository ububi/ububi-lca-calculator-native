﻿/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Francois Saab
 * Mail: saab.francois@gmail.com, francois.saab.1@ens.etsmtl.ca
 * Date: 1/1/2017
 *
 * Copyright © 2017 Francois Saab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
#ifndef ParameterUtils_H
#define ParameterUtils_H

#include <stdio.h>
#include <vector>
#include "FileUtils.hpp"
#include "../LCAModels/Exchange.hpp"
#include "../DAL/ExchangeObj.hpp"
#include "../LCAModels/CalcImpactFactor.hpp"
#include "../DAL/CalcImpactFactorObj.hpp"
#include "../LCAModels/ExchangeItem.hpp"
#include "../LCAModels/ExchangeCell.hpp"
#include "../LCAModels/CalcImpactFactorItem.hpp"

#include <sstream>
#include <time.h>
#include <iostream>

#include "../DAL/LCADB.hpp"
#include "exprtk/exprtk.hpp"
#include "../LCAModels/CalcParameter.hpp"

template <typename T>
class ParameterUtils
{

  public:
    typedef exprtk::expression<T> expression_t;

    expression_t getExpressionTable(long owner, string scope, map<string, expression_t> &expressionsTable)
    {
        expression_t expressionTable;

        string scope_id = scope + "_" + to_string(owner);

        if (expressionsTable.find(scope_id) == expressionsTable.end())
        {
            expressionTable = expressionsTable["GLOBAL_0"];
        }
        else
        {
            expressionTable = expressionsTable[scope_id];
        }

        return expressionTable;
    }

    void evalParameterTable(map<std::pair<string, string>, CalcParameter> & parameters,
                            map<string, expression_t> & expressionsTable)
    {

        typedef exprtk::symbol_table<T> symbol_table_t;

        /*< <scope_owner>, symbol_table>*/
        std::tr1::unordered_map<string, symbol_table_t> sym_tables;

        for (auto &&p : parameters)
        {
         //if(p.first.second=="x")   cout<< "p "<<p.first.second<<" "<< p.second.value <<" ; ";
        sym_tables[p.first.first /*scope_owner*/].add_variable(p.first.second /*param_name*/, p.second.value);
        }

        for (std::pair<string, symbol_table_t> symt : sym_tables)
        {
            expression_t expression;
            expression.register_symbol_table(symt.second);
            expression.register_symbol_table(sym_tables["GLOBAL_0"]);
            expressionsTable[symt.first /*scope_owner*/] = expression;
        }
    }

    // void evalParameterTable(  map<std::pair<string, string>, CalcParameter> & parameters,expression_t & expressionTable)
    // {

    //     typedef exprtk::symbol_table<T> symbol_table_t;

    //     /*<scope,symbol_table>*/
    //     symbol_table_t sym_table;

    //     for (auto &&p : parameters)
    //     {
    //         sym_table.add_variable(p.first.second, p.second.value);
    //     }

    //     expressionTable.register_symbol_table(sym_table);

    // }

    double eval(std::string formulae, expression_t &expression)
    {
        typedef exprtk::parser<T> parser_t;

        parser_t parser;
        parser.compile(formulae, expression);

        T y = expression.value();

        //if(formulae=="x+y") cout<<formulae<<" --> "<<y<<"   ;   ";

        return y;
    }
};

#endif /* ParameterUtils_H */
