#move compiled c++ to jar location, build jar, move jar into service location

root=CHANGE_ME
calculatorproject=CHANGE_ME

echo " compiling c++... "

g++ -v -pipe  -D_JNI_IMPLEMENTATION_  -m64   -fPIC \
  ./src/main/c++/Native/LCAKernel_LCAKernel.cpp \
  -I$HOME/external/libs/ \
  -I$BOOST_HOME  -std=c++17  -O0 -fopenmp -DBOOST_SYSTEM_NO_DEPRECATED \
  -I"$JAVA_HOME/include/"  \
  -I"$JAVA_HOME/include/linux/" \
  -L $BOOST_HOME/stage/lib -lgsl -lgslcblas -lm   -lboost_serialization  -shared -o libLCAKernel.so

mkdir -p ./src/main/resources/lib/Linux/amd64
cp ./libLCAKernel.so  ./src/main/resources/lib/Linux/amd64/

cd src/main/java/LCAKernel
javac LCAKernel.java
cd ..
javah LCAKernel.LCAKernel
cp LCAKernel_LCAKernel.h LCAKernel/
cd ../../..

sbt clean package
