This is the LCA calculator. It is comprised of several features each is put on a branch:

1) Mutlithreading, currently enabled on the Master branch

        compute parallel tasks using OpenMP C++. onyl basic features are enabled in this release
        
2) Multiprocessing

        Compute parallel tasks using MPI C++
        
3) Multiprocessing-Foreground-Differential

        In addition to Multiprocessing, this feature will compute based on the changes received as differential files

