logLevel := Level.Warn

resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
resolvers += "Typesafe maven repository" at "https://repo.typesafe.com/typesafe/maven-releases/"

addSbtPlugin("com.frugalmechanic" % "fm-sbt-s3-resolver" % "0.18.0")
