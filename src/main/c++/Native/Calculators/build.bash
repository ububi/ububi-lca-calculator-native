mpic++ -pipe  -D_JNI_IMPLEMENTATION_  -m64  -fPIC ./MontecarloCalculatorMain.cpp -I$HOME/libs/ -I$BOOST_HOME/  -std=c++11 -O3 -fopenmp -DBOOST_SYSTEM_NO_DEPRECATED \
            -I"$JAVA_HOME/include/"  
            -I"$JAVA_HOME/include/linux/" \
            -L$BOOST_HOME/stage/lib -lgsl -lgslcblas -lm  -lboost_mpi -lboost_serialization -march=native -DEIGEN_USE_BLAS -o processes/montecarlo
      
