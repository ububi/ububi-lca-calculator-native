/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Francois Saab
 * Mail: saab.francois@gmail.com, francois.saab.1@ens.etsmtl.ca
 * Date: 1/1/2017
 *
 * Copyright © 2017 Francois Saab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
#ifndef ContributionAnalysisReport_hpp
#define ContributionAnalysisReport_hpp

#include <stdio.h>
#include <vector>
#include "../Utilities/ResultsUtilsSingle.hpp"
#include "../Calculators/Models/AppSettings.hpp"
#include "../Calculators/Models/CalculatorData.h"
#include "../Utilities/ResultsUtilsSingle.hpp"
#include "../Calculators/Models/TwoDimStore.hpp"
#include "../LCAModels/LCAIndexes.hpp"
#include "../Calculators/SimulatorNative.hpp"
#include <omp.h>
#include "../Factories/TechnologyMatrixFactorySingle.hpp"
#include "../Factories/InterventionMatrixFactorySingle.hpp"
#include "../Factories/CharacterisationMatrixFactory.hpp"
#include "../libs/libDescriptive.hpp"
#include <chrono>
#include "../Utilities/GraphUtilSingle.hpp"
using namespace std;

class ContributionAnalysisReport
{
  public:
    AppSettings settings;
    CalculatorData *calculatorData;
    LCADB *lcadb;

    ContributionAnalysisReport(AppSettings _settings,
                               CalculatorData *_calculatorData, LCADB *_lcadb

                               )
        : calculatorData(_calculatorData), settings(_settings), lcadb(_lcadb)
    {
    }

    string printTreeNode(long key,
                         long parentNode,
                         long node,
                         long pp,
                         long p,
                         double q,
                         double total,
                         double percent,
                         double unc,
                         int impatCategoriesUnits)
    {

        std::stringstream ss;

        ss << (key)
           << (",")
           << (parentNode)
           << (",")
           << (node)
           << (",")
           << (pp)
           << (",")
           << (p)
           << (",")
           << (q) // quantity
           << (",")
           << (percent)
           << (",")
           << (total)
           << (",")
           << impatCategoriesUnits
           << (",")
           << (unc)
            //.append(",")
            //.append(if (LCADataBaseFactorySingle.impactcat_Map(key) != null) LCADataBaseFactorySingle.impactcat_Map(key).ICReferenceUnit else "Unit not found")
            //.append(if (settings.montecarlo) (","+unc) else "")
            ;

        return ss.str();
    }

    void printLCIA(GraphData *graph, SMatrix ASparse, double *lcia, Eigen::MatrixXd QBAinv,vector<ContributionTreeNode> tree)
    {
       
        /*cache local variables*/
        long Acols = (*calculatorData).lcaIndexes.ProcessesIndexLength();
        long Qrows = (*calculatorData).lcaIndexes.ImpactCategoryIndexLength();
        std::tr1::unordered_map<long, double> procs_scalar = (*graph).procs_scalar;
        vector<long> ImpactCategoryIndex = (*calculatorData).lcaIndexes.ImpactCategoryIndex;
        unordered_map<long, long> ImpactCategoryIndexIndices = (*calculatorData).lcaIndexes.ImpactCategoryIndexIndices;
        unordered_map<long, long> ProcessesIndexIndices = (*calculatorData).lcaIndexes.ProcessesIndexIndices;
        int length = (*calculatorData).lcaIndexes.ImpactCategoryIndexLength();
        vector<ImpactStat> impactStats = (*calculatorData).impactStats;
        std::tr1::unordered_map<long, long> impatCategoriesUnits = (*lcadb).impatCategoriesUnits;

        #pragma omp parallel for shared(impactStats, impatCategoriesUnits)
        for (int ii = 0; ii < length; ii++)
        {

            int cat = ImpactCategoryIndex[ii];

            std::stringstream ss;
            Eigen::MatrixXd cat_row = QBAinv.row(ImpactCategoryIndexIndices[cat]);


            for (auto &&node : tree)
            {
                long p_i = ProcessesIndexIndices[node.ProcessId];
                double scaled = ASparse.coeff(p_i, p_i) * procs_scalar[node.ProcessId];
                double merged = cat_row.coeff(p_i) * scaled * node.Share;
                double mrged_unc = -1;
                if (settings.montecarlo)
                {
                    mrged_unc = pow(scaled * node.Share, 2) * 
                    pow(impactStats[ImpactCategoryIndexIndices[cat]].std,2);
                }

                double mergedFlowQuantity = abs(merged);
                double total = lcia[ImpactCategoryIndexIndices[cat]];

                // ### akka Total Quantity of flow
                //                    if (settings.montecarlo) {
                //                    printTreeNode(cat,
                //                            node.ParentNodeId,
                //                            node.NodeId,
                //                            node.ParentProcessId,
                //                            node.ProcessId,
                //                            mergedFlowQuantity,
                //                            total,
                //                            Math.abs(mergedFlowQuantity / total) * 100,
                //
                //                            merged_unc
                //
                //                            )
                //                } else {

                ss << printTreeNode(
                          cat,
                          node.ParentNodeId,
                          node.NodeId,
                          node.ParentProcessId,
                          node.ProcessId,
                          mergedFlowQuantity,
                          total,
                          abs(mergedFlowQuantity / total) * 100,
                          mrged_unc,
                          impatCategoriesUnits[cat])
                   << endl;
                  
                //}
            }

            string lciaContributionTreePath = settings.RootPath + "data/calculations/" + settings.CalculationId + "/lcia-tree/lcia-tree_" + to_string(cat) + "_" + settings.CalculationId + ".txt";
            FileUtils fileUtils;
            fileUtils.newfolder(settings.RootPath + "data/calculations/" + settings.CalculationId + "/lcia-tree/");
            fileUtils.writeTextToFile(ss.str(), lciaContributionTreePath);
        }
    }
};

#endif /* ContributionAnalysisReport_hpp */
