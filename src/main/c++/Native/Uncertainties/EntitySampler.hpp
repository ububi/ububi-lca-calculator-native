/**---------------------------   
 * PROJECT: UBUBI
 * Auth:
 *   Francois Saab
 * Mail: saab.francois@gmail.com, francois.saab.1@ens.etsmtl.ca
 * Date: 1/1/2017
 *
 * Copyright © 2017 Francois Saab
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 *
 *--------------------------*/
#ifndef EntitySampler_hpp
#define EntitySampler_hpp

#include "../LCAModels/ExchangeItem.hpp"
#include <stdio.h>
#include "../LCAModels/CalcImpactFactorItem.hpp"
#include "Lognormal.hpp"
#include "Normal.hpp"
#include "Triangular.hpp"
#include "Uniform.hpp"
#include "../Utilities/ParameterUtils.h"
#include <iostream>
#include "omp.h"
template <typename T>
#include <functional>

class EntitySampler
{
  public:
    
    
  std::function<
         double(std::default_random_engine &, std::normal_distribution<float>)>
        f1 = Lognormal::next;

    std::function<
        double(std::default_random_engine &, std::normal_distribution<float>)>
        f2 = Normal::next;


      vector<
            std::function<
                 double(std::default_random_engine&, std::normal_distribution<float>)>>
            v_funcs{f1, f2,f1,f1};

    EntitySampler()
    {

    }

    double getCellValue(Exchange& c)
    {
        return (c.amount * c.conversionFactor * (c.input ? -1 : 1));
    }

    double getCellValue(Exchange& c, expression_t& expressiontbl)
    {
        ParameterUtils<double> paramUtils;
        double value = (c.amountFormula == "" ? c.amount : paramUtils.eval(c.amountFormula, expressiontbl));
        int sign = (c.input ? -1 : 1);
        return value * c.conversionFactor * sign;
    }

    double getNextSimulatedCellValue(ExchangeItem* c, std::default_random_engine &gen)
    {
        int sign = ((*c).exch.input ? -1 : 1);

        //return v_funcs[(*c).exch.uncertaintyType-1](gen, (*c).distribution) * (*c).exch.conversionFactor *sign;

        switch ((*c).exch.uncertaintyType)
        {
        case 1:

            return exp((*c).distribution(gen)) * (*c).exch.conversionFactor * sign;

            break;

        case 2:

            return (*c).distribution(gen) * (*c).exch.conversionFactor * sign;

            break;

        case 3:

            return Triangular::next((*c).exch.parameter1, (*c).exch.parameter2, (*c).exch.parameter3) * (*c).exch.conversionFactor * sign;

            break;

        case 4:

            return Uniform::next((*c).exch.parameter1, (*c).exch.parameter2) * (*c).exch.conversionFactor * sign;

            break;

        default:

            return (*c).exch.amount * (*c).exch.conversionFactor * sign;

            break;
        }
    }

    double getImpactCellValue(CalcImpactFactor c)
    {

        return c.amount * c.conversionFactor;
    }

    double getImpactCellValue(CalcImpactFactor c, expression_t expression)
    {

        ParameterUtils<double> paramUtils;
        string expr = c.amountFormula;
        double value = (expr == "" ? c.amount : paramUtils.eval(expr, expression));
        return value * c.conversionFactor;
    }

    double getParameterCellValue(CalcParameter p, expression_t expression)
    {

        ParameterUtils<double> paramUtils;
        double value = 0.0;
        string expr = p.formula;
        if (expr == "")
        {
            value = p.value;
        }
        else
        {
            value = paramUtils.eval(expr, expression);
        }
        int sign = 1;
        if (p.input)
        {
            sign = -1;
        }

        return value * p.conversionFactor * sign;
    }

    double getNextSimulatedParameterCellValue(CalcParameter p, std::default_random_engine gen, expression_t expression)
    {

        int sign = 1;

        if (p.uncertaintyType == 0)
        {
            return getParameterCellValue(p, expression);
        }
        else
        {
            switch (p.uncertaintyType)
            {

            case 1:
                return exp(p.distribution(gen)) * p.conversionFactor * sign;
                break;

            case 2:
                return p.distribution(gen) * p.conversionFactor * sign;
                break;

            case 3:
                return Triangular::next(p.parameter1, p.parameter2, p.parameter3) * p.conversionFactor * sign;
                break;

            case 4:
                return Uniform::next(p.parameter1, p.parameter2) * p.conversionFactor * sign;
                break;

            default:
                return p.value * p.conversionFactor * sign;
                break;
            }
        }
    }
//
//    map<string, expression_t> sampleParameterTable(ParameterTable tbl, std::default_random_engine engine, map<std::pair<string, string>, double> &parameters_sim)
//    {
//
//        typedef exprtk::symbol_table<T> symbol_table_t;
//
//        std::tr1::unordered_map<string, symbol_table_t> sym_tables;
//
//        for (auto &&p : tbl.parameters)
//        {
//
//            // symbol_table_t symbol_table;
//
//            double v = getNextSimulatedParameterCellValue(p.second, engine,
//            tbl.expressionTable//tbl.interpreters[p.first.first]
//            );
//            parameters_sim[p.first] = v;
//
//            sym_tables[p.first.first].add_variable(p.first.second, v);
//
//            //sym_tables[p.first.first].add_constants();
//        }
//
//        map<string, expression_t> interpreters;
//
//        for (std::pair<string, symbol_table_t> symt : sym_tables)
//        {
//
//            expression_t expression;
//            expression.register_symbol_table(symt.second);
//
//            expression.register_symbol_table(sym_tables["GLOBAL_0"]);
//            interpreters[symt.first] = expression;
//        }
//
//        return interpreters;
//    }
};

#endif /* EntitySampler_hpp */
