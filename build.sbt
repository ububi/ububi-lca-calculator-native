name := "UBUUBI-LCA-Calculator"

organization := "org.ets.lca"
version := "1.0"

scalaVersion := "2.11.8"
import com.amazonaws.auth.{AWSCredentialsProviderChain, DefaultAWSCredentialsProviderChain}
import com.amazonaws.auth.profile.ProfileCredentialsProvider

s3CredentialsProvider := { (bucket: String) =>
  new AWSCredentialsProviderChain(
    new ProfileCredentialsProvider("default"),
    DefaultAWSCredentialsProviderChain.getInstance()
  )
}

publishMavenStyle := true
publishTo :={
  if (isSnapshot.value) 
    Some("Ububi Snapshots" at "s3://repo.protogest.net/snapshot")
  else
    Some("Ububi Release" at "s3://repo.protogest.net/release")
}
libraryDependencies ++= Seq(

  "net.sf.trove4j" % "trove4j" % "3.0.1",
  "org.slf4j" % "slf4j-api" % "1.7.21",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.2"
)

fork in run := true

javaOptions in run ++= Seq(
  "-Xms4G", "-Xmx8G", "-XX:MaxPermSize=1024M", "-XX:+UseConcMarkSweepGC")
